package structs

import (
	"math"
	"testing"
)

const floatDifference = 0.0000000001

func checkFloatingNumber(t *testing.T, shape Shape, want, got float64) {
	t.Helper()

	difference := math.Abs(want - got)
	if difference > floatDifference {
		t.Errorf("%#v: want %g but got %g", shape, want, got)
	}
}

type TestCase struct {
	shape Shape
	want  float64
}

func TestArea(t *testing.T) {
	testcases := []TestCase{
		{Rectangle{5.0, 12.0}, 60.0},
		{Circle{10.0}, 314.1592653589793},
		{Triangle{12, 6}, 36.0},
	}

	for _, testcase := range testcases {
		got := testcase.shape.Area()
		checkFloatingNumber(t, testcase.shape, testcase.want, got)
	}
}

func TestPerimeter(t *testing.T) {
	testcases := []TestCase{
		{Rectangle{6.0, 7.0}, 26.0},
		{Circle{10.0}, 62.8318530717958},
	}

	for _, testcase := range testcases {
		got := testcase.shape.Perimeter()
		checkFloatingNumber(t, testcase.shape, testcase.want, got)
	}
}
