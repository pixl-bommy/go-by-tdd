package iteration

func Repeat(char string, repeatTimes int) string {
	var result string

	for i := 0; i < repeatTimes; i++ {
		result += char
	}

	return result
}
