package iteration

import (
	"fmt"
	"testing"
)

func TestRepeat(t *testing.T) {
	t.Run("repeating 0 times does not repeat", func(t *testing.T) {
		got := Repeat("a", 0)
		want := ""

		if got != want {
			t.Errorf("want %q but got %q", want, got)
		}
	})

	t.Run("repeating negative times does not repeat", func(t *testing.T) {
		got := Repeat("a", -20)
		want := ""

		if got != want {
			t.Errorf("want %q but got %q", want, got)
		}
	})

	t.Run("repeating 7 times repeats char 7 times", func(t *testing.T) {
		got := Repeat("a", 7)
		want := "aaaaaaa"

		if got != want {
			t.Errorf("want %q but got %q", want, got)
		}
	})
}

func ExampleRepeat() {
	repeated := Repeat("Z", 6)
	fmt.Println(repeated)
	// Output: ZZZZZZ
}

func BenchmarkRepeat(t *testing.B) {
	for i := 0; i < t.N; i++ {
		Repeat("a", 20)
	}
}
