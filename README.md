## looper

`go get -u github.com/nathany/looper`

`~/go/bin/looper`

## test

-  `go test` - current directory
-  `go test -v` - current directory with output for all tests
-  `go test -bench=.` - runs benchmark functions
-  `go test -cover` - shows test coverage
-  `go test ./... -cover` - current directory, all subdirectories and test coverage
