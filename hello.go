package main

import "fmt"

const prefixEnglish = "Hello, "

const spanish = "es"
const prefixSpanish = "Hola, "

const german = "de"
const prefixGerman = "Hallo, "

func Hello(name string, language string) string {
	if name == "" {
		name = "World"
	}

	return greetingPrefix(language) + name
}

func greetingPrefix(language string) (prefix string) {
	switch language {
	case spanish:
		prefix = prefixSpanish
	case german:
		prefix = prefixGerman
	default:
		prefix = prefixEnglish
	}

	return
}

func main() {
	fmt.Println(Hello("", ""))
}
