package main

import "testing"

func TestHello(t *testing.T) {
	assertCorrectMessage := func(t testing.TB, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got %q want %q", got, want)
		}
	}

	t.Run("empty lang -> defaults to 'en'", func(t *testing.T) {
		got := Hello("Chris", "")
		want := "Hello, Chris"
		assertCorrectMessage(t, got, want)
	})

	t.Run("empty name -> defaults to 'World'", func(t *testing.T) {
		got := Hello("", "")
		want := "Hello, World"
		assertCorrectMessage(t, got, want)
	})

	t.Run("lang 'de' -> greeting in german", func(t *testing.T) {
		got := Hello("Harry", "de")
		want := "Hallo, Harry"
		assertCorrectMessage(t, got, want)
	})

	t.Run("lang 'es' -> greeting in spanish", func(t *testing.T) {
		got := Hello("Harry", "es")
		want := "Hola, Harry"
		assertCorrectMessage(t, got, want)
	})

	t.Run("lang 'en' -> greeting in english", func(t *testing.T) {
		got := Hello("Harry", "en")
		want := "Hello, Harry"
		assertCorrectMessage(t, got, want)
	})

	t.Run("lang unknown -> greeting in english", func(t *testing.T) {
		got := Hello("Harry", "xx")
		want := "Hello, Harry"
		assertCorrectMessage(t, got, want)
	})
}
