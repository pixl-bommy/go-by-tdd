package pointers

import (
	"fmt"
	"testing"
)

func TestWallet(t *testing.T) {

	t.Run("<init>", func(t *testing.T) {
		wallet := Wallet{}
		want := Bitcoin(0)

		assertBalance(t, wallet, want)
	})

	t.Run("Deposit", func(t *testing.T) {
		wallet := Wallet{}
		wallet.Deposit(Bitcoin(10))
		want := Bitcoin(10)

		assertBalance(t, wallet, want)
	})

	t.Run("Withdraw", func(t *testing.T) {
		wallet := Wallet{100}
		err := wallet.Withdraw(Bitcoin(10))
		want := Bitcoin(90)

		assertBalance(t, wallet, want)
		assertNoError(t, err)
	})

	t.Run("Withdraw insufficient funds", func(t *testing.T) {
		startingBalance := Bitcoin(20)
		wallet := Wallet{startingBalance}

		err := wallet.Withdraw(Bitcoin(100)) // withdraw something larger than `startingBalance`

		assertBalance(t, wallet, startingBalance)
		assertError(t, err, ErrorInsufficientFunds)
	})
}

func assertBalance(t testing.TB, wallet Wallet, want Bitcoin) {
	t.Helper()
	got := wallet.Balance()

	if got != want {
		t.Errorf("want %s but got %s", want, got)
	}
}

func assertError(t testing.TB, got, want error) {
	t.Helper()
	if got == nil {
		t.Error("expected an error but there is no")
	}

	if got != want {
		t.Errorf("got %q, want %q", got, want)
	}
}

func assertNoError(t testing.TB, got error) {
	t.Helper()
	if got != nil {
		t.Error("got an error but didn't want one")
	}
}

func ExampleWallet_Balance() {
	wallet := Wallet{100}
	balance := wallet.Balance()

	fmt.Println(balance)
	// Output: 100 BTC
}

func ExampleWallet_Deposit() {
	wallet := Wallet{100}
	wallet.Deposit(Bitcoin(100))

	fmt.Println(wallet.Balance())
	// Output: 200 BTC
}

func ExampleWallet_Withdraw() {
	wallet := Wallet{100}
	err := wallet.Withdraw(10)

	if err == nil {
		fmt.Println(wallet.Balance())
	}
	// Output: 90 BTC
}

func ExampleWallet_Withdraw_withError() {
	wallet := Wallet{100}
	err := wallet.Withdraw(5000)

	if err != nil {
		fmt.Println("Error:", err)
	}
	// Output: Error: cannot withdraw, insufficient funds
}
