package mocks

import (
	"bytes"
	"fmt"
	"testing"
)

type SpySleeper struct {
	Calls int
}

func (sleeper *SpySleeper) Sleep() {
	sleeper.Calls++
}

func TestCountdown(t *testing.T) {
	buffer := &bytes.Buffer{}
	sleeper := &SpySleeper{}

	Countdown(buffer, sleeper)
	want := `3
2
1
Go!`

	assertStringEquals(t, buffer, want)

	if sleeper.Calls != 4 {
		t.Errorf("sleeper should be called 4 times but was called %d times", sleeper.Calls)
	}
}

func assertStringEquals(t testing.TB, got fmt.Stringer, want string) {
	t.Helper()

	gotString := got.String()

	if gotString != want {
		t.Errorf("want %q but got %q", want, gotString)
	}
}
