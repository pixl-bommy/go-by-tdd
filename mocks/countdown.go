package mocks

import (
	"fmt"
	"io"
	"os"
	"time"
)

const (
	finalWord      = "Go!"
	countdownStart = 3
)

type Sleeper interface {
	Sleep()
}

type SecondSleeper struct {
	SleepingTime time.Duration
}

func (sleeper *SecondSleeper) Sleep() {
	time.Sleep(sleeper.SleepingTime * time.Second)
}

func Countdown(out io.Writer, sleeper Sleeper) {
	for i := countdownStart; i > 0; i-- {
		sleeper.Sleep()
		fmt.Fprintln(out, i)
	}
	sleeper.Sleep()
	fmt.Fprint(out, finalWord)
}

func CountdownEachSecond() {
	sleeper := &SecondSleeper{1}
	Countdown(os.Stdout, sleeper)
}
