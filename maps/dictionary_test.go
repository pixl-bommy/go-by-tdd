package maps

import (
	"testing"
)

func TestSearch(t *testing.T) {
	dictionary := Dictionary{"test": "this is just a test"}

	t.Run("known word", func(t *testing.T) {
		key := "test"
		got, _ := dictionary.Search(key)
		want := "this is just a test"

		assertMapStrings(t, key, got, want)
	})

	t.Run("unknown word", func(t *testing.T) {
		key := "unknown"
		_, err := dictionary.Search(key)

		assertMapError(t, key, err, ErrorKeyDoesNotExists)
	})
}

func TestAdd(t *testing.T) {
	t.Run("add an new entry", func(t *testing.T) {
		dictionary := Dictionary{}

		key := "test"
		value := "this is just a test"
		action := dictionary.Add(key, value)

		assertDefinition(t, dictionary, key, value)
		assertStrings(t, action, ActionAdded)
	})

	t.Run("a key that already is in dictionary will be overwritten", func(t *testing.T) {
		dictionary := Dictionary{}

		key := "test"
		dictionary.Add(key, "this is just a test")

		value := "something new"
		action := dictionary.Add(key, value)

		assertDefinition(t, dictionary, key, value)
		assertStrings(t, action, ActionUpdated)
	})
}

func TestDelete(t *testing.T) {
	t.Run("delete an existing entry", func(t *testing.T) {
		key := "test"
		value := "this is just a test"
		dictionary := Dictionary{key: value}

		err := dictionary.Delete(key)

		if err != nil {
			t.Error("expected no error but there is one")
		}

		_, errSearch := dictionary.Search(key)
		assertMapError(t, key, errSearch, ErrorKeyDoesNotExists)
	})

	t.Run("delete an entry that does not exists", func(t *testing.T) {
		dictionary := Dictionary{}
		key := "test"

		err := dictionary.Delete(key)

		if err == nil {
			t.Error("expected an error but there is no")
		}

		_, errSearch := dictionary.Search(key)
		assertMapError(t, key, errSearch, ErrorKeyDoesNotExists)
	})
}

func assertStrings(t testing.TB, got, want string) {
	t.Helper()

	if got != want {
		t.Errorf("got %q but want %q", got, want)
	}
}

func assertMapStrings(t testing.TB, key, got, want string) {
	t.Helper()

	if got != want {
		t.Errorf("key %q: got %q but want %q", key, got, want)
	}
}

func assertMapError(t testing.TB, key string, got, want error) {
	t.Helper()
	if got == nil {
		t.Error("expected an error but there is no")
	}

	if got != want {
		t.Errorf("got %q, want %q", got, want)
	}
}

func assertDefinition(t testing.TB, dictionary Dictionary, key, value string) {
	t.Helper()

	got, err := dictionary.Search(key)
	if err != nil {
		t.Fatal("should find added key:", err)
	}

	if value != got {
		t.Errorf("got %q but want %q", got, value)
	}
}
