package maps

const (
	ErrorKeyDoesNotExists = DictionaryError("could not find key you were looking for")
)

const (
	ActionAdded   = "added"
	ActionUpdated = "updated"
)

type Dictionary map[string]string

func (dict Dictionary) Search(key string) (string, error) {
	value, ok := dict[key]

	if !ok {
		return "", ErrorKeyDoesNotExists
	}

	return value, nil
}

func (dict Dictionary) Add(key, value string) (action string) {
	_, ok := dict[key]
	dict[key] = value

	if ok {
		return ActionUpdated
	}
	return ActionAdded
}

func (dict Dictionary) Delete(key string) error {
	_, ok := dict[key]

	if !ok {
		return ErrorKeyDoesNotExists
	}

	delete(dict, key)
	return nil
}

type DictionaryError string

func (err DictionaryError) Error() string {
	return string(err)
}
