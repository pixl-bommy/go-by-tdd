package concurrency

type WebsiteChecker func(string) bool
type result struct {
	string
	bool
}

func CheckWebsites(wc WebsiteChecker, list []string) map[string]bool {
	websites := make(map[string]bool)
	resultChannel := make(chan result)

	for _, website := range list {
		go func(url string) {
			resultChannel <- result{url, wc(url)}
		}(website)
	}

	for i := 0; i < len(list); i++ {
		r := <-resultChannel
		websites[r.string] = r.bool
	}

	return websites
}
