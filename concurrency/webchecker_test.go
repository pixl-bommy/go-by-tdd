package concurrency

import (
	"reflect"
	"testing"
	"time"
)

func mockWebsitChecker(url string) bool {
	return url != "err://homepage.me"
}

func slowWebsiteChecker(_ string) bool {
	time.Sleep(20 * time.Millisecond)
	return true
}

func TestCheckWebsites(t *testing.T) {
	websites := []string{
		"http://google.de",
		"http://harry.net",
		"err://homepage.me",
		"https://haz.net",
	}

	want := map[string]bool{
		"http://google.de":  true,
		"http://harry.net":  true,
		"err://homepage.me": false,
		"https://haz.net":   true,
	}

	got := CheckWebsites(mockWebsitChecker, websites)

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("want %v but got %v", want, got)
	}
}

func BenchmarkCheckWebsites(b *testing.B) {
	urls := make([]string, 100)
	for i := 0; i < len(urls); i++ {
		urls[i] = "a url"
	}

	for i := 0; i < b.N; i++ {
		CheckWebsites(slowWebsiteChecker, urls)
	}
}
