package injection

import (
	"bytes"
	"fmt"
	"testing"
)

func TestGreet(t *testing.T) {
	buffer := bytes.Buffer{}

	Greet(&buffer, "Harry")
	want := "Hello, Harry"

	assertStringEquals(t, &buffer, want)
}

func assertStringEquals(t testing.TB, got fmt.Stringer, want string) {
	t.Helper()

	gotString := got.String()

	if gotString != want {
		t.Errorf("want %q but got %q", want, gotString)
	}
}
