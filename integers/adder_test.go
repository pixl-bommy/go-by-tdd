package integers

import (
	"fmt"
	"testing"
)

func TestAdd(t *testing.T) {
	got := Add(2, 3)
	want := 5

	if got != want {
		t.Errorf("expected %d but got %d", want, got)
	}
}

func ExampleAdd() {
	sum := Add(1, 17)
	fmt.Println(sum)
	// Output: 18
}
