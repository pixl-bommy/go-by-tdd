package integers

func Sum(list []int) (sum int) {
	for _, number := range list {
		sum += number
	}
	return
}

func SumAll(lists [][]int) (sum int) {
	for _, list := range lists {
		sum += Sum(list)
	}
	return
}

func SumTuple(lists ...[]int) (tuples []int) {
	for _, list := range lists {
		tuples = append(tuples, Sum(list))
	}
	return
}
