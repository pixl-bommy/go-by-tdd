package integers

import (
	"fmt"
	"reflect"
	"testing"
)

func TestSum(t *testing.T) {
	checkSumWithExpected := func(t testing.TB, list []int, want int) {
		t.Helper()
		got := Sum(list)
		if got != want {
			t.Errorf("want %d but got %d", want, got)
		}
	}

	t.Run("sum five numbers -> correct sum", func(t *testing.T) {
		setup := []int{1, 2, 3, 4, 5}
		want := 15

		checkSumWithExpected(t, setup, want)
	})

	t.Run("sum empty list -> 0", func(t *testing.T) {
		setup := []int{}
		want := 0

		checkSumWithExpected(t, setup, want)
	})

	t.Run("sum negative numbers -> correct sum", func(t *testing.T) {
		setup := []int{-2, -4, -6}
		want := -12

		checkSumWithExpected(t, setup, want)
	})
}

func TestSumAll(t *testing.T) {
	checkSumWithExpected := func(t testing.TB, lists [][]int, want int) {
		t.Helper()
		got := SumAll(lists)
		if got != want {
			t.Errorf("want %d but got %d", want, got)
		}
	}

	t.Run("sum one list with five numbers -> correct sum", func(t *testing.T) {
		setup := [][]int{{1, 2, 3, 4, 5}}
		want := 15

		checkSumWithExpected(t, setup, want)
	})

	t.Run("sum list of empty lists -> 0", func(t *testing.T) {
		setup := [][]int{}
		want := 0

		checkSumWithExpected(t, setup, want)
	})

	t.Run("sum two lists with five numbers -> correct sum", func(t *testing.T) {
		setup := [][]int{{1, 2, 3}, {4, 5}}
		want := 15

		checkSumWithExpected(t, setup, want)
	})
}

func TestSumTuple(t *testing.T) {
	checkSumWithExpected := func(t testing.TB, lists [][]int, want []int) {
		t.Helper()
		got := SumTuple(lists...)
		if !reflect.DeepEqual(got, want) {
			t.Errorf("want %v but got %v", want, got)
		}
	}

	t.Run("sum one list with five numbers -> correct sums", func(t *testing.T) {
		setup := [][]int{{1, 2, 3, 4, 5}}
		want := []int{15}

		checkSumWithExpected(t, setup, want)
	})

	t.Run("sum two lists with five numbers -> correct sums", func(t *testing.T) {
		setup := [][]int{{1, 2, 3}, {4, 5}}
		want := []int{6, 9}

		checkSumWithExpected(t, setup, want)
	})

	t.Run("sum list of empty lists -> empty list", func(t *testing.T) {
		want := []int{}
		got := SumTuple()

		if len(got) != 0 || len(got) != len(want) {
			t.Errorf("want %v but got %v", want, got)
		}
	})
}

func ExampleSum() {
	sum := Sum([]int{2, 4, 6})
	fmt.Println(sum)
	// Output: 12
}

func ExampleSumAll() {
	sum := SumAll([][]int{{2, 4, 6}, {8, 10}})
	fmt.Println(sum)
	// Output: 30
}

func ExampleSumTuple() {
	sum := SumTuple([]int{2, 4, 6}, []int{8, 10})
	fmt.Println(sum)
	// Output: [12 18]
}
